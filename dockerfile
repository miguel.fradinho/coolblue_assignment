FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build-env
WORKDIR /src/xyz.mnsf.WebApi

# Copy everything
COPY  . ./

# Restore as distinct layers
RUN dotnet restore
# Build and publish a release
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:7.0
WORKDIR /src/xyz.mnsf.WebApi
COPY --from=build-env /src/xyz.mnsf.WebApi/out .
ENTRYPOINT ["dotnet", "xyz.mnsf.WebApi.dll"]
