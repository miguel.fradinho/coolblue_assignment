﻿using xyz.mnsf.WebApi.Models;

namespace xyz.mnsf.WebApi.Services.Interfaces;

/// <summary>
///     Service for handling logic related to <see cref="ProductType"/>
/// </summary>
public interface IProductTypeService
{
    /// <summary>
    ///     Gets the <see cref="ProductType"/> with matching Id
    /// </summary>
    /// <exception cref="Exceptions.ProductTypeException">
    ///     Exception thrown when the type does not exist
    /// </exception>
    Task<ProductType> GetProductTypeById(int id);

    /// <summary>
    ///     Gets all the <see cref="ProductType"/>s
    /// </summary>
    Task<IEnumerable<ProductType>> GetAllProductTypes();
}
