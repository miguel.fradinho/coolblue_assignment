﻿using Newtonsoft.Json;

namespace xyz.mnsf.WebApi.Models;

/// <summary>
///     Domain model for <see cref="ProductType"/>
/// </summary>
public class ProductType
{
    /// <summary>
    ///     Id
    /// </summary>
    [JsonProperty("id")]
    public int Id { get; set; }

    /// <summary>
    ///     Name of the Type
    /// </summary>
    public string Name { get; set; } = null!;
    
    /// <summary>
    ///     Flag to calculating the insurance
    /// </summary>
    public bool CanBeInsured { get; set; }

    public override bool Equals(object? obj)
    {
        return obj is ProductType type &&
               Id == type.Id &&
               Name == type.Name &&
               CanBeInsured == type.CanBeInsured;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Id, Name, CanBeInsured);
    }
}
