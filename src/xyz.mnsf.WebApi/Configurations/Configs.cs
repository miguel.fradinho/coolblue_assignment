﻿namespace xyz.mnsf.WebApi.Configurations;

/// <summary>
///     Constants to be used throughout the program + for ease of access in creating the tests (1 single point of change)
/// </summary>
public static class Configs
{
    #region Insurance Values
    public static readonly int LOWER_BRACKET_THRESHOLD = 500;
    public static readonly int UPPER_BRACKET_THRESHOLD = 2000;

    public static readonly int LOWER_BRACKET_INSURANCE = 0;
    public static readonly int MIDDLE_BRACKET_INSURANCE = 1000;
    public static readonly int UPPER_BRACKET_INSURANCE = 2000;

    public static readonly int EXTRA_INSURANCE = 500;
    #endregion
}
