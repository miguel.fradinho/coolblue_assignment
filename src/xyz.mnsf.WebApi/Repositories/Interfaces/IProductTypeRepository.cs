﻿using xyz.mnsf.WebApi.Models;

namespace xyz.mnsf.WebApi.Repositories.Interfaces;

/// <summary>
///     Repository for handling operations that manipulate the <see cref="ProductType"/> domain model
/// </summary>
public interface IProductTypeRepository : IReadRepository<ProductType, int>
{ }
