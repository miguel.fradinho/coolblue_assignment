﻿using AutoFixture;
using FluentAssertions;
using Moq;
using System.Net;
using xyz.mnsf.WebApi.Exceptions;
using xyz.mnsf.WebApi.Models;
using xyz.mnsf.WebApi.Repositories.Interfaces;
using xyz.mnsf.WebApi.Services;

namespace xys.mnsf.Infrastructure.Tests.Services;

public class ProductTypeServiceTests
{
    private readonly ProductTypeService _productService;
    private readonly Mock<IProductTypeRepository> _mockRepo;

    private readonly IFixture _fixture = new Fixture();

    public ProductTypeServiceTests()
    {
        _mockRepo = new Mock<IProductTypeRepository>();
        _productService = new ProductTypeService(_mockRepo.Object);
    }

    [Fact]
    public async Task GetById_ShouldReturnType_WhenProductTypeExists()
    {
        // Setup
        ProductType? type = _fixture.Create<ProductType>();

        _mockRepo.Setup(r => r.GetById(type.Id)).ReturnsAsync(type);

        // Call
        ProductType getProduct = await _productService.GetProductTypeById(type.Id);

        // Assert
        getProduct.Should().NotBeNull().And.BeSameAs(type);

        _mockRepo.VerifyAll();
    }

    [Fact]
    public async Task GetById_ShouldThrowException_WhenProductTypeDoesNotExist()
    {
        //Setup
        ProductType? type = _fixture.Create<ProductType>();

        ProductTypeException exception = new(HttpStatusCode.NotFound, $"The {type.GetType().Name} with {{Id={type.Id}}} does not exist");

        _mockRepo.Setup(r => r.GetById(type.Id)).ReturnsAsync((ProductType?)null);

        // Call
        Func<Task> getCall = async () => await _productService.GetProductTypeById(type.Id);

        // Assert
        await getCall.Should().ThrowAsync<ProductTypeException>()
            .Where(e => e.ErrorCode == HttpStatusCode.NotFound)
            .Where(e => e.Title == exception.Title)
            .WithMessage(exception.Message);

        _mockRepo.VerifyAll();
    }
}
