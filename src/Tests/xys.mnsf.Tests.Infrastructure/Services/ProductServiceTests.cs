﻿using AutoFixture;
using FluentAssertions;
using Moq;
using System.Net;
using xyz.mnsf.WebApi.Exceptions;
using xyz.mnsf.WebApi.Models;
using xyz.mnsf.WebApi.Repositories.Interfaces;
using xyz.mnsf.WebApi.Services;

namespace xys.mnsf.Infrastructure.Tests.Services;

public class ProductServiceTests
{
    private readonly ProductService _productService;
    private readonly Mock<IProductRepository> _mockRepo;

    private readonly IFixture _fixture = new Fixture();

    public ProductServiceTests()
    {
        _mockRepo = new Mock<IProductRepository>();
        _productService = new ProductService(_mockRepo.Object);
    }


    [Fact]
    public async Task GetById_ShouldReturnProduct_WhenProductExists()
    {
        // Setup
        Product? product = _fixture.Create<Product>();

        _mockRepo.Setup(r => r.GetById(product.Id)).ReturnsAsync(product);

        // Call
        Product getProduct = await _productService.GetProductById(product.Id);

        // Assert
        getProduct.Should().NotBeNull().And.BeSameAs(product);

        _mockRepo.VerifyAll();
    }

    [Fact]
    public async Task GetById_ShouldThrowException_WhenProductDoesNotExist()
    {
        //Setup
        Product? product = _fixture.Create<Product>();

        ProductException exception = new(HttpStatusCode.NotFound, $"The {product.GetType().Name} with {{Id={product.Id}}} does not exist");
   
        _mockRepo.Setup(r => r.GetById(product.Id)).ReturnsAsync((Product?)null);

        // Call
        Func<Task> getCall = async () => await _productService.GetProductById(product.Id);

        // Assert
        await getCall.Should().ThrowAsync<ProductException>()
            .Where(e => e.ErrorCode == HttpStatusCode.NotFound)
            .Where(e => e.Title == exception.Title)
            .WithMessage(exception.Message);

        _mockRepo.VerifyAll();
    }
}
