﻿using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Moq;
using xyz.mnsf.WebApi.Controllers;
using xyz.mnsf.WebApi.Exceptions;
using xyz.mnsf.WebApi.Models;
using xyz.mnsf.WebApi.Services.Interfaces;

namespace xyz.mnsf.Tests.WebApi.Controllers;

public class ProductControllerTests
{

    private readonly ProductController _productController;
    private readonly Mock<IInsuranceService> _insuranceServiceMock;
    private readonly Mock<IProductService> _productServiceMock;
    private readonly IFixture _fixture = new Fixture();


    public ProductControllerTests()
    {
        _productServiceMock = new Mock<IProductService>();
        _insuranceServiceMock = new Mock<IInsuranceService>();
        _productController = new ProductController(_insuranceServiceMock.Object, _productServiceMock.Object)
        {
            ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext(),
            }
        };
    }

    [Fact]
    public async Task GetById_ShouldReturnOk_WhenProductExists()
    {
        // Setup
        var product = _fixture.Create<Product>();

        _productServiceMock.Setup(r => r.GetProductById(product.Id)).ReturnsAsync(product);

        // Call
        var response = await _productController.GetProductById(product.Id);

        // Assert
        response.Value.Should().BeSameAs(product);
        _productServiceMock.VerifyAll();
    }

    [Fact]
    public async Task GetById_ShouldReturnNotFound_WhenProductDoesntExist()
    {
        // Setup
        var product = _fixture.Create<Product>();

        var exception = _fixture.Create<ProductException>();

        _productServiceMock.Setup(r => r.GetProductById(product.Id)).ThrowsAsync(exception);

        // Call
        var response = await _productController.GetProductById(product.Id);

        // Assert
        response.Result.Should().BeOfType<NotFoundResult>();
        _productServiceMock.VerifyAll();
    }

    [Theory]
    [InlineData(0)]
    public async Task GetAll_ShouldReturnOk_RegardlessOfObjects(int numberOfProducts)
    {
        // Setup
        var products = _fixture.CreateMany<Product>(numberOfProducts);

        _productServiceMock.Setup(r => r.GetAllProducts()).ReturnsAsync(products);

        // Call
        var response = await _productController.GetAllProducts();

        // Assert
        response.Value.Should().ContainInOrder(products);
    }

    [Fact]
    public async Task CalculateInsurance_ShouldReturnNotFound_WhenProductDoesntExist()
    {
        // Setup
        var product = _fixture.Create<Product>();

        var exception = _fixture.Create<ProductException>();

        _productServiceMock.Setup(r => r.GetProductById(product.Id)).ThrowsAsync(exception);

        // Call
        var response = await _productController.CalculateInsuranceCost(product.Id);

        // Assert
        response.Result.Should().BeOfType<NotFoundResult>();
        _productServiceMock.VerifyAll();
    }

    [Fact]
    public async Task CalculateInsurance_ShouldReturnBadRequest_WhenTypeCantBeInsured()
    {
        // Setup
        var product = _fixture.Create<Product>();
        var exception = _fixture.Create<InsuranceException>();

        _productServiceMock.Setup(r => r.GetProductById(product.Id)).ReturnsAsync(product);
        _insuranceServiceMock.Setup(r => r.CalculateInternalInsuranceForProduct(product)).ThrowsAsync(exception);

        // Call
        var response = await _productController.CalculateInsuranceCost(product.Id);

        // Assert
        response.Result.Should().BeOfType<BadRequestResult>();
        _productServiceMock.VerifyAll();
        _insuranceServiceMock.VerifyAll();
    }

    [Theory]
    [InlineData(10)]
    public async Task CalculateInsurance_ShouldReturnOk_WhenProductCanBeInsured(int cost)
    {

        // Setup
        var product = _fixture.Create<Product>();

        _productServiceMock.Setup(r => r.GetProductById(product.Id)).ReturnsAsync(product);
        _insuranceServiceMock.Setup(r => r.CalculateInternalInsuranceForProduct(product)).ReturnsAsync(cost);

        // Call
        var response = await _productController.CalculateInsuranceCost(product.Id);

        // Assert
        response.Value.Should().Be(cost);
        _productServiceMock.VerifyAll();
        _insuranceServiceMock.VerifyAll();
    }
}
