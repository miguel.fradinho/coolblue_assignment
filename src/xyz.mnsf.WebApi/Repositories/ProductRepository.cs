﻿using Microsoft.EntityFrameworkCore;
using xyz.mnsf.WebApi.Models;
using xyz.mnsf.WebApi.Persistence;
using xyz.mnsf.WebApi.Repositories.Interfaces;

namespace xyz.mnsf.WebApi.Repositories;

/// <summary>
///     <inheritdoc cref="IProductRepository"/>
/// </summary>
public class ProductRepository : IProductRepository
{
    private readonly ApplicationDbContext _context;

    public ProductRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Product?> GetById(int id)
    {
        return await _context.FindAsync<Product>(id);
    }

    public async Task<IEnumerable<Product>> GetAll()
    {
        return await _context.Products.ToListAsync();
    }
}
