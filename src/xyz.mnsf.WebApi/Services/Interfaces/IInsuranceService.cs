﻿using xyz.mnsf.WebApi.Models;

namespace xyz.mnsf.WebApi.Services.Interfaces;

/// <summary>
///     Internal Service to handle the logic for calculating Insurances. 
///     Not directly exposed.
/// </summary>
public interface IInsuranceService
{
    /// <summary>
    ///     Calculates the Insurance cost for the given <see cref="Models.Product"/> 
    /// </summary>
    /// <exception cref="Exceptions.InsuranceException">
    ///     Exception thrown when called with a Product which Type cannot be insured
    /// </exception>
    Task<int> CalculateInternalInsuranceForProduct(Product product);
}