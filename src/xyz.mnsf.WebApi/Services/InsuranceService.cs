﻿using xyz.mnsf.WebApi.Configurations;
using xyz.mnsf.WebApi.Exceptions;
using xyz.mnsf.WebApi.Models;
using xyz.mnsf.WebApi.Services.Interfaces;

namespace xyz.mnsf.WebApi.Services;

/// <summary>
///     <inheritdoc cref="IInsuranceService"/>
/// </summary>
public class InsuranceService : IInsuranceService
{
    private readonly IProductTypeService _productTypeService;

    public InsuranceService(IProductTypeService productTypeService)
    {
        _productTypeService = productTypeService;
    }

    public async Task<int> CalculateInternalInsuranceForProduct(Product product)
    {
        // We don't need to validate Product here
        // nor check if type exists/expect an exception 
        // because we already have an exist Product here (and a Product, in practice, can't exist without a matching type)
        
        // So, just get the corresponding type
        // Check if the product can be insured
        var type = await _productTypeService.GetProductTypeById(product.ProductTypeId);

        // Handling edge cases 
        if (!type.CanBeInsured)
        {
            throw InsuranceException.CannotBeInsured;
        }


        // Handling insurance calculation
        int insurance_value;
        if (product.SalesPrice < Configs.LOWER_BRACKET_THRESHOLD)
        {
            insurance_value = Configs.LOWER_BRACKET_INSURANCE;
        }
        else if (product.SalesPrice >= Configs.LOWER_BRACKET_THRESHOLD && product.SalesPrice < Configs.UPPER_BRACKET_THRESHOLD)
        {
            insurance_value = Configs.MIDDLE_BRACKET_INSURANCE;
        }
        // product.SalesPrice >= UPPER_BRACKET_THRESHOLD
        else
        {
            insurance_value = Configs.UPPER_BRACKET_INSURANCE;
        }

        // The most correct way would be to have a comparison with an Enum, however that would require some overhead in our data modelling 
        // (as currently we're loading from the Web Representation of it, not the domain)

        /* Edge case of similar names but different insurance policies is handled above
         * See also id 21 and 841
         */

        // Realistically, we don't need to check for the canBeInsured attribute, since we already did above
        if (type.CanBeInsured)
        {
            switch (type.Name)
            {
                case "Laptops":
                case "Smartphones":
                    insurance_value += Configs.EXTRA_INSURANCE;
                    break;
                default:
                    break;
            }
        }
        return insurance_value;
    }

}
