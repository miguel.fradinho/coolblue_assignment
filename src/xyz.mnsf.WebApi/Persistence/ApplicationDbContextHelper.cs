﻿namespace xyz.mnsf.WebApi.Persistence;

public static class ApplicationDbContextHelper
{
    /// <summary>
    ///     For having sample data available when running the app
    /// </summary>
    public static async Task SeedSampleData(IHost host)
    {
        using IServiceScope scope = host.Services.CreateScope();
        IServiceProvider services = scope.ServiceProvider;

        ApplicationDbContext context = services.GetRequiredService<ApplicationDbContext>();
        await ApplicationDbContextSeed.SeedSampleDataAsync(context);
    }
}
