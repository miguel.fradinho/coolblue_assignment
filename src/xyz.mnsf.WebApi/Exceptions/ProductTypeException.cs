﻿using System.Net;
using System.Runtime.Serialization;

namespace xyz.mnsf.WebApi.Exceptions;

/// <summary>
///     Custom exception for handling errors related to the model <see cref="Models.ProductType"/>
/// </summary>
[Serializable]
public class ProductTypeException : BaseApiException
{

    public ProductTypeException(HttpStatusCode errorCode, string message) : base(errorCode, message)
    {
    }

    protected ProductTypeException(SerializationInfo info, StreamingContext context) : base(info, context)
    { }

    protected ProductTypeException(string message) : base(message)
    { }
}
