﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;

namespace xyz.mnsf.WebApi.Controllers;

/// <summary>
///     Custom base class so we're can attributes of all of our controllers
/// </summary>
[ApiController]
[Produces(MediaTypeNames.Application.Json)]
public abstract class ApiControllerBase : ControllerBase
{ }