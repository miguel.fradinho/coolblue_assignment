﻿using xyz.mnsf.WebApi.Models;

namespace xyz.mnsf.WebApi.Repositories.Interfaces;

/// <summary>
///     Repository for handling operations that manipulate the <see cref="Product"/> domain model
/// </summary>
public interface IProductRepository : IReadRepository<Product, int>
{ }
