﻿using Newtonsoft.Json;

namespace xyz.mnsf.WebApi.Models;

/// <summary>
///     Domain model for <see cref="Product"/>
/// </summary>
public class Product
{
    /// <summary>
    ///     Id
    /// </summary>
    [JsonProperty("id")]
    public int Id { get; set; }

    /// <summary>
    ///     Price of the product
    /// </summary>
    public float SalesPrice { get; set; }

    /// <summary>
    ///     Name of the Product
    /// </summary>
    public string Name { get; set; } = null!;

    /// <summary>
    ///     FK for the ProductType
    /// </summary>
    [JsonProperty("productTypeId")]
    public int ProductTypeId { get; set; }

    public override bool Equals(object? obj)
    {
        return obj is Product product &&
               Id == product.Id &&
               SalesPrice == product.SalesPrice &&
               Name == product.Name &&
               ProductTypeId == product.ProductTypeId;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Id, SalesPrice, Name, ProductTypeId);
    }
}
