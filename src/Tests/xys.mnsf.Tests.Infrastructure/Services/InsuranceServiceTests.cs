﻿using AutoFixture;
using FluentAssertions;
using Moq;
using System.Net;
using xyz.mnsf.WebApi.Configurations;
using xyz.mnsf.WebApi.Exceptions;
using xyz.mnsf.WebApi.Models;
using xyz.mnsf.WebApi.Services;
using xyz.mnsf.WebApi.Services.Interfaces;

namespace xys.mnsf.Infrastructure.Tests.Services;

public class InsuranceServiceTests
{
    private readonly InsuranceService _insuranceService;
    private readonly Mock<IProductTypeService> _mockTypeService;

    private readonly IFixture _fixture = new Fixture();

    public InsuranceServiceTests()
    {
        _mockTypeService = new Mock<IProductTypeService>();

        _insuranceService = new InsuranceService(_mockTypeService.Object);
    }

    [Fact]
    public async Task InsuranceCost_ShouldThrowBadRequest_WhenProductTypeCantBeRequested()
    {
        //Setup
        var product = _fixture.Create<Product>();
        var type = _fixture.Create<ProductType>();
        // make it so the has the matching product type 
        product.ProductTypeId = type.Id;
        // change the created type so it can't be insured
        type.CanBeInsured = false;

        _mockTypeService.Setup(r => r.GetProductTypeById(type.Id)).ReturnsAsync(type);

        var exception = InsuranceException.CannotBeInsured;

        // Call
        Func<Task> getCall = async () => await _insuranceService.CalculateInternalInsuranceForProduct(product);

        // Assert
        await getCall.Should().ThrowAsync<InsuranceException>()
            .Where(e => e.ErrorCode == HttpStatusCode.BadRequest)
            .Where(e => e.Title == exception.Title)
            .WithMessage(exception.Message);

        _mockTypeService.VerifyAll();
    }

    [Fact]
    public async Task InsuranceCost_ShouldReturn0_WhenProductPriceBelowLowerThreshold()
    {
        // Setup
        var product = _fixture.Create<Product>();
        var type = _fixture.Create<ProductType>();
        product.ProductTypeId = type.Id;
        type.CanBeInsured = true;

        product.SalesPrice = Configs.LOWER_BRACKET_THRESHOLD - 1;

        _mockTypeService.Setup(r => r.GetProductTypeById(type.Id)).ReturnsAsync(type);

        // Call
        int insuranceCost = await _insuranceService.CalculateInternalInsuranceForProduct(product);

        // Assert
        insuranceCost.Should().Be(0);

        _mockTypeService.VerifyAll();
    }

    [Fact]
    public async Task InsuranceCost_ShouldReturnMiddleBracketCost_WhenPriceInMiddleBracket()
    {
        // Setup
        var product = _fixture.Create<Product>();
        var type = _fixture.Create<ProductType>();
        product.ProductTypeId = type.Id;
        type.CanBeInsured = true;

        product.SalesPrice = Configs.LOWER_BRACKET_THRESHOLD + 1;

        _mockTypeService.Setup(r => r.GetProductTypeById(type.Id)).ReturnsAsync(type);
        // Call
        int insuranceCost = await _insuranceService.CalculateInternalInsuranceForProduct(product);

        // Assert
        insuranceCost.Should().Be(Configs.MIDDLE_BRACKET_INSURANCE);

        _mockTypeService.VerifyAll();
    }


    [Fact]
    public async Task InsuranceCost_ShouldReturnMaxCost_WhenPriceExceedsUpperThreshold()
    {
        // Setup
        var product = _fixture.Create<Product>();
        var type = _fixture.Create<ProductType>();
        product.ProductTypeId = type.Id;
        type.CanBeInsured = true;

        product.SalesPrice = Configs.UPPER_BRACKET_THRESHOLD + 1;

        _mockTypeService.Setup(r => r.GetProductTypeById(type.Id)).ReturnsAsync(type);
        // Call
        int insuranceCost = await _insuranceService.CalculateInternalInsuranceForProduct(product);

        // Assert
        insuranceCost.Should().Be(Configs.UPPER_BRACKET_INSURANCE);

        _mockTypeService.VerifyAll();
    }

    [Fact]
    public async Task InsuranceCost_ShouldHandleSpecialInsurance_WhenPriceBelowLowerThreshold()
    {
        // Setup
        var type = _fixture.Create<ProductType>();
        type.CanBeInsured = true;
        type.Name = "Laptops"; // Example special case
        _mockTypeService.Setup(r => r.GetProductTypeById(type.Id)).ReturnsAsync(type);

        var product = _fixture.Create<Product>();
        product.ProductTypeId = type.Id;

        product.SalesPrice = Configs.LOWER_BRACKET_THRESHOLD - 1;

        // Call
        int insuranceCost = await _insuranceService.CalculateInternalInsuranceForProduct(product);

        // Assert
        insuranceCost.Should().Be(Configs.LOWER_BRACKET_INSURANCE + Configs.EXTRA_INSURANCE);

        _mockTypeService.VerifyAll();
    }

    [Fact]
    public async Task InsuranceCost_ShouldHandleSpecialInsurance_WhenPriceInMiddleBracket()
    {
        // Setup
        var type = _fixture.Create<ProductType>();
        type.CanBeInsured = true;
        type.Name = "Laptops"; // Example special case
        _mockTypeService.Setup(r => r.GetProductTypeById(type.Id)).ReturnsAsync(type);

        var product = _fixture.Create<Product>();
        product.ProductTypeId = type.Id;

        product.SalesPrice = Configs.LOWER_BRACKET_THRESHOLD + 1;

        // Call
        int insuranceCost = await _insuranceService.CalculateInternalInsuranceForProduct(product);

        // Assert
        insuranceCost.Should().Be(Configs.MIDDLE_BRACKET_INSURANCE + Configs.EXTRA_INSURANCE);

        _mockTypeService.VerifyAll();
    }

    [Fact]
    public async Task InsuranceCost_ShouldHandleSpecialInsurance_UpperThreshold()
    {
        // Setup
        var type = _fixture.Create<ProductType>();
        type.CanBeInsured = true;
        type.Name = "Laptops"; // Example special case
        _mockTypeService.Setup(r => r.GetProductTypeById(type.Id)).ReturnsAsync(type);

        var product = _fixture.Create<Product>();
        product.ProductTypeId = type.Id;

        product.SalesPrice = Configs.UPPER_BRACKET_THRESHOLD + 1;

        // Call
        int insuranceCost = await _insuranceService.CalculateInternalInsuranceForProduct(product);

        // Assert
        insuranceCost.Should().Be(Configs.UPPER_BRACKET_INSURANCE + Configs.EXTRA_INSURANCE);

        _mockTypeService.VerifyAll();
    }

    [Fact]
    public async Task InsuranceCost_ShouldIgnoreSpecial_WhenTypeCantBeInsured()
    {
        // Setup
        var type = _fixture.Create<ProductType>();
        type.CanBeInsured = false;
        type.Name = "Laptops"; // Example special case
        _mockTypeService.Setup(r => r.GetProductTypeById(type.Id)).ReturnsAsync(type);

        var product = _fixture.Create<Product>();
        product.ProductTypeId = type.Id;

        var exception = InsuranceException.CannotBeInsured;

        // Call
        Func<Task> getCall = async () => await _insuranceService.CalculateInternalInsuranceForProduct(product);

        // Assert
        await getCall.Should().ThrowAsync<InsuranceException>()
            .Where(e => e.ErrorCode == HttpStatusCode.BadRequest)
            .Where(e => e.Title == exception.Title)
            .WithMessage(exception.Message);

        _mockTypeService.VerifyAll();
    }
}
