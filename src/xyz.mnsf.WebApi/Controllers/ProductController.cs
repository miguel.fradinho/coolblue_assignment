﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using xyz.mnsf.WebApi.Exceptions;
using xyz.mnsf.WebApi.Models;
using xyz.mnsf.WebApi.Services.Interfaces;

namespace xyz.mnsf.WebApi.Controllers;

/// <summary>
///     Endpoints related to <see cref="Product"/>, including insurance calculation
/// </summary>
[Route("api/[controller]")]
public class ProductController : ApiControllerBase
{
    private readonly IProductService _productsService;

    private readonly IInsuranceService _insuranceService;

    public ProductController(
        IInsuranceService insuranceService,
        IProductService productsService)
    {
        _productsService = productsService;
        _insuranceService = insuranceService;
    }

    [HttpGet]
    [SwaggerOperation(Summary = "Get all Products", OperationId = "GetAllProducts")]
    [SwaggerResponse(StatusCodes.Status200OK, "All Products were retrieved successfully", typeof(IEnumerable<Product>))]
    public async Task<ActionResult<IList<Product>>> GetAllProducts()
    {
        var products = await _productsService.GetAllProducts();

        return products.ToList();
    }

    [HttpGet("{id}")]
    [SwaggerOperation(Summary = "Get a Product by Id", OperationId = "GetProductById")]
    [SwaggerResponse(StatusCodes.Status200OK, "The Product with the given Id was retrieved succesfully", typeof(Product))]
    [SwaggerResponse(StatusCodes.Status404NotFound, "The Product with the provided ID does not exist")]
    public async Task<ActionResult<Product>> GetProductById(int id)
    {
        try
        {
            var product = await _productsService.GetProductById(id);

            return product;
        }
        catch (ProductException)
        {
            return NotFound();
        }
    }

    [HttpGet("{id}/insurance_cost")]
    [SwaggerOperation(Summary = "Get a Product by Id", OperationId = "GetProductById")]
    [SwaggerResponse(StatusCodes.Status200OK, "The insurance calculation was sucessful", typeof(int))]
    [SwaggerResponse(StatusCodes.Status404NotFound, "The Product with the provided Id does not exist")]
    [SwaggerResponse(StatusCodes.Status400BadRequest, "The Product provided cannot be insured!")]
    public async Task<ActionResult<int>> CalculateInsuranceCost(int id)
    {

        // Get the product and exit earlier if it's not found
        Product product;
        try
        {
            product = await _productsService.GetProductById(id);
        }
        catch (ProductException)
        {
            return NotFound();
        }

        try
        {
            var total_cost = await _insuranceService.CalculateInternalInsuranceForProduct(product);
            return total_cost;
        }
        catch (InsuranceException)
        {
            return BadRequest();
        }
    }
}
