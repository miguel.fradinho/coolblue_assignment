﻿using System.Net;
using xyz.mnsf.WebApi.Exceptions;
using xyz.mnsf.WebApi.Models;
using xyz.mnsf.WebApi.Repositories.Interfaces;
using xyz.mnsf.WebApi.Services.Interfaces;

namespace xyz.mnsf.WebApi.Services;

/// <summary>
///     <inheritdoc cref="IProductService"/>
/// </summary>
public class ProductService : IProductService
{

    private readonly IProductRepository _productRepository;

    public ProductService(IProductRepository productRepository)
    {
        _productRepository = productRepository;
    }

    public async Task<Product> GetProductById(int id)
    {
        var product = await _productRepository.GetById(id);

        if (product == null)
        {
            throw new ProductException(HttpStatusCode.NotFound, $"The {nameof(Product)} with {{Id={id}}} does not exist");
        }
        return product;
    }

    public async Task<IEnumerable<Product>> GetAllProducts()
    {
        var products = await _productRepository.GetAll();
        return products;
    }

}
