using Microsoft.EntityFrameworkCore;
using xyz.mnsf.WebApi.Persistence;
using xyz.mnsf.WebApi.Repositories;
using xyz.mnsf.WebApi.Repositories.Interfaces;
using xyz.mnsf.WebApi.Services;
using xyz.mnsf.WebApi.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);

builder.Services
    .AddDbContext<ApplicationDbContext>(options =>
        options.UseInMemoryDatabase("xyz.mnsf")
    )
    .AddScoped<IProductRepository, ProductRepository>()
    .AddScoped<IProductTypeRepository, ProductTypeRepository>()
    .AddScoped<IProductService, ProductService>()
    .AddScoped<IProductTypeService, ProductTypeService>()
    .AddScoped<IInsuranceService, InsuranceService>()
    .AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(swagger =>
    {
        
        swagger.UseOneOfForPolymorphism();

        swagger.UseAllOfForInheritance();

        swagger.UseAllOfToExtendReferenceSchemas();

        swagger.SupportNonNullableReferenceTypes();

        swagger.EnableAnnotations();
    }
);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();

    app.UseDeveloperExceptionPage();

    // So we can test through swagger
    await ApplicationDbContextHelper.SeedSampleData(app);
}

app.UseHttpsRedirection();

app.UseAuthorization();
app.MapControllers();


app.Run();
