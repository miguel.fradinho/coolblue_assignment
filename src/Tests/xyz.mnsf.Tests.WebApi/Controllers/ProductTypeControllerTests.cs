﻿using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using xyz.mnsf.WebApi.Controllers;
using xyz.mnsf.WebApi.Exceptions;
using xyz.mnsf.WebApi.Models;
using xyz.mnsf.WebApi.Services.Interfaces;

namespace xyz.mnsf.Tests.WebApi.Controllers;

public class ProductTypeControllerTests
{

    private readonly ProductTypeController _productTypeController;
    private readonly Mock<IProductTypeService> _typeServiceMock;
    private readonly IFixture _fixture = new Fixture();


    public ProductTypeControllerTests()
    {
        _typeServiceMock = new Mock<IProductTypeService>();
        _productTypeController = new ProductTypeController(_typeServiceMock.Object)
        {
            ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext(),
            }
        };
    }

    [Fact]
    public async Task GetById_ShouldReturnOk_WhenProductTypeExists()
    {
        // Setup
        var type = _fixture.Create<ProductType>();
        
        _typeServiceMock.Setup(r => r.GetProductTypeById(type.Id)).ReturnsAsync(type);

        // Call
        var response = await _productTypeController.GetById(type.Id);

        // Assert
        response.Value.Should().BeSameAs(type);
        _typeServiceMock.VerifyAll();
    }

    [Fact]
    public async Task GetById_ShouldReturnNotFound_WhenProductTypeDoesntExist()
    {
        // Setup
        var type = _fixture.Create<ProductType>();

        var exception = _fixture.Create<ProductTypeException>();

        _typeServiceMock.Setup(r => r.GetProductTypeById(type.Id)).ThrowsAsync(exception);

        // Call
        var response = await _productTypeController.GetById(type.Id);

        // Assert
        response.Result.Should().BeOfType<NotFoundResult>();
        _typeServiceMock.VerifyAll();
    }

    [Theory]
    [InlineData(0)]
    public async Task GetAll_ShouldReturnOk_RegardlessOfObjects(int numberOfTypes)
    {
        // Setup
        var types = _fixture.CreateMany<ProductType>(numberOfTypes);

        _typeServiceMock.Setup(r => r.GetAllProductTypes()).ReturnsAsync(types);

        // Call
        var response = await _productTypeController.GetAll();

        // Assert
        response.Value.Should().ContainInOrder(types);
    }
}
