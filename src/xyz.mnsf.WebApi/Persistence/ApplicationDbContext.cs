﻿using Microsoft.EntityFrameworkCore;
using xyz.mnsf.WebApi.Models;

namespace xyz.mnsf.WebApi.Persistence;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext() { }

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

    public virtual DbSet<Product> Products { get; set; } = null!;

    public virtual DbSet<ProductType> ProductTypes { get; set; } = null!;
}
