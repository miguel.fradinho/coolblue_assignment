﻿using System.Net;
using System.Runtime.Serialization;

namespace xyz.mnsf.WebApi.Exceptions;

/// <summary>
///     Custom exception for handling errors related to the model <see cref="Models.Product"/>
/// </summary>
[Serializable]
public class ProductException : BaseApiException
{

    public ProductException(HttpStatusCode errorCode, string message) : base(errorCode, message)
    {
    }

    protected ProductException(SerializationInfo info, StreamingContext context) : base(info, context)
    { }

    protected ProductException(string message) : base(message)
    { }
}
