﻿using Microsoft.EntityFrameworkCore;
using xyz.mnsf.WebApi.Models;
using xyz.mnsf.WebApi.Persistence;
using xyz.mnsf.WebApi.Repositories.Interfaces;

namespace xyz.mnsf.WebApi.Repositories;

/// <summary>
///     <inheritdoc cref="IProductTypeRepository"/>
/// </summary>
public class ProductTypeRepository : IProductTypeRepository
{
    private readonly ApplicationDbContext _context;

    public ProductTypeRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<ProductType?> GetById(int id)
    {
        return await _context.FindAsync<ProductType>(id);
    }

    public async Task<IEnumerable<ProductType>> GetAll()
    {
        return await _context.ProductTypes.ToListAsync();
    }
}
