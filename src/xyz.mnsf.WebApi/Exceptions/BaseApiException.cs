﻿using Newtonsoft.Json;
using System.Net;
using System.Runtime.Serialization;

namespace xyz.mnsf.WebApi.Exceptions;

/// <summary>
///     Custom class intended to be as simplified version of https://tools.ietf.org/html/rfc7807
///     It unifies the errors in using the API in a strongly-typed format.
///     Currently not fully implemented.
/// </summary>
[Serializable]
public abstract class BaseApiException : Exception
{
    public string Title { get; } = null!;

    /// <summary>
    ///     HTTP Error code that represents what caused this exception
    /// </summary>
    public HttpStatusCode ErrorCode { get; }

    protected BaseApiException(HttpStatusCode errorCode, string message) : base(message)
    {
        Title = "API exception";
        ErrorCode = errorCode;
    }
    protected BaseApiException(string title, HttpStatusCode errorCode, string message) : base(message)
    {
        Title = title;
        ErrorCode = errorCode;
    }


    protected BaseApiException(SerializationInfo info, StreamingContext context) : base(info, context)
    { }

    protected BaseApiException(string message) : base(message)
    { }
}
