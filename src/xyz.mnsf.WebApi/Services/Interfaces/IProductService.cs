﻿using xyz.mnsf.WebApi.Models;

namespace xyz.mnsf.WebApi.Services.Interfaces;
/// <summary>
///     Service for handling logic related to <see cref="Product"/>
/// </summary>
public interface IProductService
{
    /// <summary>
    ///     Gets the <see cref="Product"/> with matching Id
    /// </summary>
    /// <exception cref="Exceptions.ProductException">
    ///     Exception thrown when the product does not exist
    /// </exception>
    Task<Product> GetProductById(int id);

    /// <summary>
    ///     Gets all the <see cref="Product"/>s
    /// </summary>
    Task<IEnumerable<Product>> GetAllProducts();
}
