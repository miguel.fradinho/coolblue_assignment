﻿using System.Net;
using System.Runtime.Serialization;

namespace xyz.mnsf.WebApi.Exceptions;

/// <summary>
///     Custom exception to throw for errors related to operations with calculating insurance, in  <see cref="Services.InsuranceService"/>
/// </summary>
[Serializable]
public class InsuranceException : BaseApiException
{
    /// <summary>
    ///     Factory Method for single point of change so it can be reused elsewhere
    /// </summary>
    public static InsuranceException CannotBeInsured => new(HttpStatusCode.BadRequest, "This productType cannot be insured");


    public InsuranceException(HttpStatusCode errorCode, string message) : base(errorCode, message)
    {
    }

    protected InsuranceException(SerializationInfo info, StreamingContext context) : base(info, context)
    { }

    protected InsuranceException(string message) : base(message)
    { }
}
