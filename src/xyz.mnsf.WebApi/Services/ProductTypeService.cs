﻿using System.Net;
using xyz.mnsf.WebApi.Exceptions;
using xyz.mnsf.WebApi.Models;
using xyz.mnsf.WebApi.Repositories.Interfaces;
using xyz.mnsf.WebApi.Services.Interfaces;

namespace xyz.mnsf.WebApi.Services;

/// <summary>
///     <inheritdoc cref="IProductTypeService"/>
/// </summary>
public class ProductTypeService : IProductTypeService
{

    private readonly IProductTypeRepository _productTypeRepository;

    public ProductTypeService(IProductTypeRepository productTypeRepository)
    {
        _productTypeRepository = productTypeRepository;
    }
    public async Task<ProductType> GetProductTypeById(int id)
    {
        var productType = await _productTypeRepository.GetById(id);

        if (productType == null)
        {
            throw new ProductTypeException(HttpStatusCode.NotFound, $"The {nameof(ProductType)} with {{Id={id}}} does not exist");
        }
        return productType;
    }

    public async Task<IEnumerable<ProductType>> GetAllProductTypes()
    {
        var types = await _productTypeRepository.GetAll();
        return types;
    }
}
