﻿namespace xyz.mnsf.WebApi.Repositories.Interfaces;

/// <summary>
///     Generic read repository interface
/// </summary>
public interface IReadRepository<TType, in TKey>
{
    /// <summary>
    ///     Gets the specified <see cref="TType"/> entity based on the <paramref name="id"/>
    /// </summary>
    Task<TType?> GetById(TKey id);

    /// <summary>
    ///     Retrieves a list of the specified <see cref="TType"/> entity 
    /// </summary>
    Task<IEnumerable<TType>> GetAll();
}
