﻿using Newtonsoft.Json;
using xyz.mnsf.WebApi.Models;

namespace xyz.mnsf.WebApi.Persistence;

/// <summary>
///     Class for seeding <see cref="ApplicationDbContext"/>
/// </summary>
public static class ApplicationDbContextSeed
{

    /// <summary>
    ///     Loads sample data for <see cref="Product"/> and <see cref="ProductType"/>
    /// </summary>
    public static async Task SeedSampleDataAsync(ApplicationDbContext context)
    {
        context.Database.EnsureDeleted();

        var products = ProductSample();
        context.Products.AddRange(products);

        var productTypes = ProductTypesSample();
        context.ProductTypes.AddRange(productTypes);

        await context.SaveChangesAsync();
    }

    private static IList<Product> ProductSample()
    {
        var path = Path.Join(Environment.CurrentDirectory, "Products.json");
        var products = LoadJson<List<Product>>(path);
        return products ?? new List<Product>();
    }

    private static IList<ProductType> ProductTypesSample()
    {
        var path = Path.Join(Environment.CurrentDirectory, "ProductTypes.json");
        var productTypes = LoadJson<List<ProductType>>(path);
        return productTypes ?? new List<ProductType>();
    }

    private static T? LoadJson<T>(string fileName)
    {
        var content = File.ReadAllText(fileName);

        if (string.IsNullOrWhiteSpace(content))
        {
            return default;
        }

        var products = JsonConvert.DeserializeObject<T>(content);

        return products ?? default;
    }
}

