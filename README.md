# Sidenotes
- Currently, the Dockerfile only serves to test the build, as due to incorrect configurations, it's not accessible from outside
- The `docker-compose` only serves to run the sonarqube instance locally (pgadmin for ease of configuring the sonar user, rather than using a script)
- `.gitlab-ci.yml` is gitlab's built-in security analyzer that runs on every commit (creating a CI/CD pipeline to build the project, despite possible, was not the scope of this assignment)

# In-Memory Database
Our main goal is to test the application (a Web API), and not the implementation of an already existing catalogue of products (that might be managed elsewhere).
So we can setup an in-memory database.

# Lack of relationships
Both due to current implementation + separation of concerns.
We could map relationships directly so we'd have automatic population by EFCore on them (e.g. direct mapping from `ProductType` attribute to respective `ProductType`).

While this makes it easier to work rather than having to make another query, I don't feel like it'd be the correct approach.

Having our objects fully hydrated would make them harder to cache (on a separate level), while also making any future changes have to change the entire structure (due to the inherent relational structure), instead of changing simple ID attributes. 

The trade-off is that the mapping must be done on application level, but this is already done & supported to some degree by several FE frameworks.

Additionally, it also allows portatiblity as now as we could have completely difference sources from several datatypes (e.g. in this example it doesn't really make sense, but if we were talking about assets, like images, we shouldn't really be hydrating the object with that, it's much easier to work with the ID/file path, instead of caching that binary object, since otherwise we'd be duplicating the work of the CDN)

\+ A good example of the complications of mapping relationships are ProductTypes 21 and 841, where if we were to classify as "equal" entities, considering "CanBeInsured" is a flag and the ID is auto-generated, thus the only real attribute would be the name, they'd be the same entity, despite being conceptually different 


# Repository Layer
Rational for repository layer follows the Microsoft guidance of present on [Microsoft EF Core documentation](https://learn.microsoft.com/en-us/ef/core/testing/choosing-a-testing-strategy#repository-pattern)

# Serialization of objects
The best way would have been to create 2 separate objects (one for Domain, one for the Web API), and converting between them.
However, for simplicity of implementation (since we're preloading the DB), we've gone with just selecting the relative attributes


# Insurance Service
Which methods to expose from the Insurance Service can be tricky, will be handled during interview.
Same for the bracket logics

# Testing
All tests present are Unit Tests.
Testing approach will be explained during the interview