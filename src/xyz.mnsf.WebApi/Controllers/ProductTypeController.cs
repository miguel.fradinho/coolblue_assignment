﻿using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using xyz.mnsf.WebApi.Exceptions;
using xyz.mnsf.WebApi.Models;
using xyz.mnsf.WebApi.Services.Interfaces;

namespace xyz.mnsf.WebApi.Controllers;

/// <summary>
///     Endpoints related to <see cref="ProductType"/>
/// </summary>
[Route("api/[controller]")]
public class ProductTypeController : ApiControllerBase
{
    private readonly IProductTypeService _productTypeService;

    public ProductTypeController(IProductTypeService productTypeService)
    {
        _productTypeService = productTypeService;
    }

    [HttpGet]
    [SwaggerOperation(Summary = "Get all Product Types", OperationId = "GetAllProductTypes")]
    [SwaggerResponse(StatusCodes.Status200OK, "All ProductsTypes were retrieved successfully", typeof(IEnumerable<ProductType>))]
    public async Task<ActionResult<IList<ProductType>>> GetAll()
    {
        var types = await _productTypeService.GetAllProductTypes();

        return types.ToList();
    }

    [HttpGet("{id}")]
    [SwaggerOperation(Summary = "Get a ProductType by Id", OperationId = "GetProductTypeById")]
    [SwaggerResponse(StatusCodes.Status200OK, "The ProductType with the given Id was retrieved succesfully", typeof(ProductType))]
    [SwaggerResponse(StatusCodes.Status404NotFound, "The ProductType with the provided ID does not exist")]
    public async Task<ActionResult<ProductType>> GetById(int id)
    {
        try
        {
            var type = await _productTypeService.GetProductTypeById(id);

            return type;
        }
        catch (ProductTypeException)
        {
            return NotFound();
        }
    }
}
